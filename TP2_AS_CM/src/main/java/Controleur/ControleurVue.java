package Controleur;

import java.awt.Desktop;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import Modele.ExceptionPersonnalisee;
import Modele.Machine;
import Modele.UtilitaireCommunication;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class ControleurVue implements Initializable {

	private Machine machine;
	FileChooser fileChooser;
	Desktop desktop;
	public static ObservableList<String> observableListMessage = FXCollections.observableArrayList();
	public static ObservableList<String> observableListInformations = FXCollections.observableArrayList();

	@FXML
	private AnchorPane fileChooserId;

	@FXML
	private TextField textFieldIpDistant;

	@FXML
	private TextField textFieldPortDistantId;

	@FXML
	private Button boutonConnectionId;

	@FXML
	private TextField textFieldUserNameId;

	@FXML
	private TextField textFieldMessageAEnvoyer;

	@FXML
	private Button boutonEnvoyerMessageId;

	@FXML
	private Button boutonQuitterId;

	@FXML
	private TextField textFieldFichierAEnvoyerId;

	@FXML
	private Button boutonEnvoyerFichierId;

	@FXML
	private Button boutonFileChooserId;

	@FXML
	private ListView<String> listtViewMessage;

	@FXML
	private ListView<String> listViewInformation;

	@FXML
	void connecterOnAction(ActionEvent event) {
		if (!textFieldIpDistant.getText().isEmpty() && !textFieldUserNameId.getText().isEmpty() && !textFieldPortDistantId.getText().isEmpty()) {
			machine = new Machine(textFieldUserNameId.getText());
			machine.boutonConnexion(textFieldIpDistant.getText(), Integer.parseInt(textFieldPortDistantId.getText()),
					textFieldUserNameId.getText());

			ChangeListener<? super Number> listener = new ChangeListener<Number>() {

				public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
					if (newValue.intValue() == 1 || newValue.intValue() == 2) {
						boutonConnectionId.setDisable(true);
						boutonEnvoyerMessageId.setDisable(false);
						boutonEnvoyerFichierId.setDisable(false);
						boutonFileChooserId.setDisable(false);
					}
				}
			};
			machine.getMode().addListener(listener);
		}

	}

	@FXML
	void envoyerFichierOnAction(ActionEvent event) {
		if (!textFieldFichierAEnvoyerId.getText().isEmpty()) {
			if (machine.getMode().getValue().intValue() == 1) {
				machine.getClientFichierCourant().envoiFichier(textFieldFichierAEnvoyerId.getText());
			} else if (machine.getMode().getValue().intValue() == 2) {
				machine.getServeurFichierCourant().envoiFichier(textFieldFichierAEnvoyerId.getText());
			}
		} else {
			new ExceptionPersonnalisee("Erreur fichier", "Veuiller entrer le chemin d'un fichier svp");
		}
		textFieldFichierAEnvoyerId.setText("");
	}

	@FXML
	void envoyerMessageOnAction(ActionEvent event) {
		if (!textFieldMessageAEnvoyer.getText().isEmpty()) {
			if (machine.getMode().getValue().intValue() == 1) {
				machine.getClientMessagerieCourant().envoyerMessage(textFieldMessageAEnvoyer.getText());
			} else if (machine.getMode().getValue().intValue() == 2) {
				machine.getServeurMessagerieCourant().envoyerMessage(textFieldMessageAEnvoyer.getText());
			}
			textFieldMessageAEnvoyer.setText("");
		} else {

		}
	}

	@FXML
	void fileOnChooserOnAction(ActionEvent event) {
		List<File> list =
                fileChooser.showOpenMultipleDialog(new Stage());
            if (list != null) {
                list.stream().forEach((file) -> {
                    getAbsolutePath(file);
                });
            }
	}

	private void getAbsolutePath(File file) {
		Platform.runLater(()->{
			textFieldFichierAEnvoyerId.setText(file.getAbsolutePath());
		});
	}

	@FXML
	void quitterOnAction(ActionEvent event) {
		UtilitaireCommunication.closeConnection(UtilitaireCommunication.elementsDeConnexion);
		Platform.exit();
		System.exit(0);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fileChooser = new FileChooser();
		desktop = Desktop.getDesktop();
		boutonEnvoyerMessageId.setDisable(true);
		boutonEnvoyerFichierId.setDisable(true);
		boutonFileChooserId.setDisable(true);
		textFieldIpDistant.setText("10.10.10.1");
		textFieldPortDistantId.setText("44444");
		textFieldUserNameId.setText("Akim");

		listViewInformation.setItems(observableListInformations);
		listtViewMessage.setItems(observableListMessage);

	}

}
