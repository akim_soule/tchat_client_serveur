package Modele;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Classe representant le serveur de messagerie.
 * Elle a pour principal attribbut le nom du user,
 * un PrintWriter pour l'envoi de message,
 * un BufferedReader pour la lecture des messages.
 * @author akimsoule
 *
 */
public class ServeurMessagerie {

	/**
	 * Le nom du user.
	 */
	private String nomUser;

	/**
	 * Le socket de communication.
	 */
	private Socket socket;
	/**
	 * Le PrintWriter pour l'envoi de message
	 */
	private PrintWriter printOut;
	/**
	 * Le BufferedReader pour la lecture des messages.
	 */
	private BufferedReader in;

	/**
	 * Le constructeur du serveur de messagerie, envoyant une notification au client de messagerie
	 * et recevant un message de ce dernier.
	 * @param pClientSocket
	 * Le socket de communication.
	 * @param pNomUser
	 * Le nom du user.
	 */
	public ServeurMessagerie(Socket pClientSocket, String pNomUser) {
		socket = pClientSocket;
		nomUser = pNomUser;
		try {
			printOut = new PrintWriter(socket.getOutputStream());
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			UtilitaireCommunication.notification(printOut, nomUser);
			UtilitaireCommunication.recevoirNotification(in, printOut);

			receptionMessage();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Methode permettant d'envoyer un message.
	 * @param msg
	 * Le message a envoyer.
	 */
	public void envoyerMessage(String msg) {
		final String message = msg;
		new Thread(new Runnable() {
			public void run() {
				UtilitaireCommunication.envoyerMessage(printOut, message, nomUser);
			}
		}).start();

	}

	/**
	 * Methode permettant de recenoir un message.
	 */
	public void receptionMessage() {
		new Thread(new Runnable() {

			public void run() {
				UtilitaireCommunication.receptionMessage(in, printOut);
			}

		}).start();
	}

}
