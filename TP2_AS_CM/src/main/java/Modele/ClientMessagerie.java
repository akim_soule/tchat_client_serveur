package Modele;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Classe permettant de definir un client de messagerie.
 * Elle a pour principal attribut le nom du user,
 * le socket de transmission, un printWriter pour l'ecriture et un
 * bufferedReader pour la lecture.
 * @author akimsoule
 *
 */
public class ClientMessagerie {

	/**
	 * Le nom du user.
	 */
	private String nomUser;
	/**
	 * Le socket de transmission.
	 */
	private Socket socket;
	/**
	 * Le PrintWriter pour l'envoi de message.
	 */
	private PrintWriter printOut;
	/**
	 * Le BufferedReader pour la lecture.
	 */
	private BufferedReader in;

	/**
	 * Constructeur permettant de creer un client de messagerie.
	 * Elle envoie une notification pour signaler qu'elle est connectee et
	 * recoit un notification pour afficher le client connecte.
	 * Elle est en mode reception de message.
	 * @param pClientSocket
	 * Le socket de transmission.
	 * @param pNomUser
	 * Le nom du user.
	 */
	public ClientMessagerie(Socket pClientSocket, String pNomUser) {
		socket = pClientSocket;
		nomUser = pNomUser;
		try {
			printOut = new PrintWriter(socket.getOutputStream());
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

				UtilitaireCommunication.notification(printOut, nomUser);
				UtilitaireCommunication.recevoirNotification(in, printOut);


			receptionMessage();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	/**
	 * Methode permettant d'envoyer les messages.
	 * @param msg
	 * C'est le message
	 */
	public void envoyerMessage(final String msg) {
		new Thread(new Runnable() {
			public void run() {
				UtilitaireCommunication.envoyerMessage(printOut, msg, nomUser);
			}
		}).start();

	}

	/**
	 * Methode permettant de recenoir les messages.
	 */
	public void receptionMessage() {
		new Thread(new Runnable() {

			public void run() {
				UtilitaireCommunication.receptionMessage(in, printOut);
			}

		}).start();
	}


}
