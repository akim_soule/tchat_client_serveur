package Modele;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * Classe permettant de personnaliser l'exception rencontree.
 * Elle a pour principal attribut le titre et le message de
 * la fenetre Alert.
 * @author akimsoule
 *
 */
public class ExceptionPersonnalisee extends Exception {

	private static final long serialVersionUID = 1L;
	/**
	 * Le titre du l'erreur.
	 */
	String titre;
	/**
	 * Le message de l'erreur.
	 */
	String message;

	/**
	 * La classe permettant de creer une exception.
	 * @param titre
	 * le titre de l'alert.
	 * @param message
	 * Le message de l'alert.
	 */
	public ExceptionPersonnalisee(String titre, String message) {
		super();
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Erreur");
		alert.setHeaderText(titre);
		alert.setContentText(message);

		alert.showAndWait();
	}





}
