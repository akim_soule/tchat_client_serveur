package Modele;

/**
 * Classe permettant de definir le chemin de destination
 * 
 * @author akimsoule
 *
 */
public class CheminFichier {

	/**
	 * Le chemin de destination
	 */
	public static String CHEMIN_DESTINATION = "C:\\temp\\destination\\";

	public static String CHEMIN_DESTINATION_MAC = "/Users/" + System.getProperty("user.name") + "/Documents/destination/";

}
