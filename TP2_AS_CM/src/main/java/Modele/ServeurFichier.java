package Modele;


import java.net.Socket;

/**
 * Cette classe represente le serveur de fichier.
 * Elle a pour principal attribut le socket de communication.
 * @author akimsoule
 *
 */
public class ServeurFichier {

	/**
	 * Le socket de communication.
	 */
	private Socket socket;

	/**
	 * Le constructeur de la classe avec pour parametre le socket
	 * et le nom du user.
	 * @param pClientSocket
	 * Le socket de communication.
	 * @param pNomUser
	 * Le nom du user.
	 */
	public ServeurFichier(Socket pClientSocket, String pNomUser) {
		socket = pClientSocket;
		receptionFichier();
	}

	/**
	 * Methode permettant d'envoyer un fichier.
	 * @param chemin
	 * Le chemin absolu du fichier.
	 */
	public void envoiFichier(String chemin) {
		final String finalChemin = chemin;
		new Thread(new Runnable() {
			public void run() {
				UtilitaireCommunication.envoyerFichier(finalChemin, socket);
				;
			}
		}).start();;
	}


	/**
	 * Methode permettant de recevoir le fichier.
	 */
	public void receptionFichier() {
		new Thread(new Runnable() {
			public void run() {
				UtilitaireCommunication.receptionFichier(socket);
			}
		}).start();;
	}

}
