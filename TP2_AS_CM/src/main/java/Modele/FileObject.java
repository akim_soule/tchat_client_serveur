package Modele;

import java.io.Serializable;
import java.util.regex.Pattern;

/**
 * Classe permettant de definir un fichier. Elle a pour principal attribut, le
 * chemin source absolu, le chemin receptable, le chemin de destination absolu,
 * le nom du fichier, sa taille, les donnees ainsi qu'un boolean indiquant si
 * l'objet est utilisale ou pas.
 * 
 * @author akimsoule
 *
 */
public class FileObject implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Le chemin absolu source du fichier.
	 */
	private String cheminAbsoluSource;
	/**
	 * Le chemin receptacle absolu de destination.
	 */
	private String cheminAbsoluDestinationReceptacle;
	/**
	 * Le chemin absolu de destination.
	 */
	private String cheminAbsoluDestination;
	/**
	 * Le nom du fichier.
	 */
	private String nomFichier;
	/**
	 * La taille du fichier.
	 */
	private long taille;
	/**
	 * Les donnees du fichier.
	 */
	private byte[] donnees;
	/**
	 * Boolean indiquant si la contruction de l'objet est utilisable ou pas.
	 */
	private boolean reussiteTransformationObjet;

	/**
	 * Le constructeur de la classe permettant de definir le chemin absolu du
	 * fichier, Le chemin absolu de reception du fichier ainsi que le chemin absolu
	 * de destination du fichier.
	 * 
	 * @param pCheminAbsoluSource
	 */
	public FileObject(String pCheminAbsoluSource) {
		cheminAbsoluSource = pCheminAbsoluSource;
		if (cheminAbsoluSource.contains("\\") || cheminAbsoluSource.contains("/")) {
			String pattern = Pattern.quote(System.getProperty("file.separator"));
			nomFichier = cheminAbsoluSource.split(pattern)[cheminAbsoluSource.split(pattern).length - 1];
			if (System.getProperty("os.name").equals("Mac OS X")) {
				cheminAbsoluDestinationReceptacle = CheminFichier.CHEMIN_DESTINATION_MAC;
				cheminAbsoluDestination = CheminFichier.CHEMIN_DESTINATION_MAC + "/" + nomFichier;
			}else {
				cheminAbsoluDestinationReceptacle = CheminFichier.CHEMIN_DESTINATION;
				cheminAbsoluDestination = CheminFichier.CHEMIN_DESTINATION + "\\" + nomFichier;
			}
			
		}

	}

	public String getCheminAbsoluSource() {
		return cheminAbsoluSource;
	}

	public void setCheminAbsoluSource(String cheminAbsoluSource) {
		this.cheminAbsoluSource = cheminAbsoluSource;
	}

	public String getCheminAbsoluDestinationReceptacle() {
		return cheminAbsoluDestinationReceptacle;
	}

	public void setCheminAbsoluDestinationReceptacle(String cheminAbsoluDestinationReceptacle) {
		this.cheminAbsoluDestinationReceptacle = cheminAbsoluDestinationReceptacle;
	}

	public String getCheminAbsoluDestination() {
		return cheminAbsoluDestination;
	}

	public void setCheminAbsoluDestination(String cheminAbsoluDestination) {
		this.cheminAbsoluDestination = cheminAbsoluDestination;
	}

	public String getNomFichier() {
		return nomFichier;
	}

	public void setNomFichier(String nomFichier) {
		this.nomFichier = nomFichier;
	}

	public long getTaille() {
		return taille;
	}

	public void setTaille(long taille) {
		this.taille = taille;
	}

	public byte[] getDonnees() {
		return donnees;
	}

	public void setDonnees(byte[] donnees) {
		this.donnees = donnees;
	}

	public boolean isReussiteTransformationObjet() {
		return reussiteTransformationObjet;
	}

	public void setReussiteTransformationObjet(boolean reussiteTransformationObjet) {
		this.reussiteTransformationObjet = reussiteTransformationObjet;
	}

}
