package Modele;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import Controleur.ControleurVue;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * Classe permettant de definir une machine ou un ordinateur.
 * Elle determine le mode de connection : soit client, soit serveur.
 * Elle a pour principal attribut un socket,
 * le nom de l'utilisateur,
 * un serveur socket pour le mode serveur
 * un serveur de messagerie,
 * un client de messagerie,
 * un client de fichier et
 * un serveur de fichier.
 * @author akimsoule
 *
 */
public class Machine {

	/**
	 * Le nom de l'utilisateur.
	 */
	private String userName;

	/**
	 * Le serveur socket.
	 */
	private ServerSocket serveurSocket;

	/**
	 * Le socket pour la transmission de message.
	 */
	private Socket socket;
	/**
	 * Le socket fichier.
	 */
	private Socket socketFichier;
	/**
	 * Le client de messagerie.
	 */
	private ClientMessagerie clientMessagerieCourant;
	/**
	 * Le serveur de messagerie.
	 */
	private ServeurMessagerie serveurMessagerieCourant;
	/**
	 * Le client de fichier.
	 */
	private ClientFichier clientFichierCourant;
	/**
	 * Le serveur de fichier.
	 */
	private ServeurFichier serveurFichierCourant;

	/**
	 * Le mode de connection, lequel sera ecoute par le controleur indiquant le mode de
	 * connexion de la machine.
	 */
	private IntegerProperty mode = new SimpleIntegerProperty(0);

	/**
	 * La construction d'une machine avec le nom de l'utilisateur.
	 * @param puserName
	 * Le nom de l'utilisateur.
	 */
	public Machine(String puserName) {
		setUserName(puserName);
	}

	/**
	 * Gestion du bouton connexion.
	 * @param ipDistant
	 * L'IP distant.
	 * @param portDistant
	 * Le port distant
	 * @param pNomUser
	 * Le nom du user.
	 */
	public void boutonConnexion(final String ipDistant, final int portDistant, final String pNomUser) {
		Platform.runLater(new Runnable() {
			public void run() {
				ControleurVue.observableListInformations.clear();
			}
		});
		Thread threadBoutonConnexion = new Thread(new Runnable() {

			public void run() {
				Platform.runLater(new Runnable() {
					public void run() {
						ControleurVue.observableListInformations.add("Serveur en attente sur le port " + portDistant);
					}
				});
				try {
					/**
					 * Tentative de se connecter en mode client a un serveur.
					 */
					socket = new Socket(ipDistant, portDistant);

					socketFichier = new Socket(ipDistant, portDistant);

					mode.set(1);

					clientMessagerieCourant = new ClientMessagerie(socket, pNomUser);
					clientFichierCourant = new ClientFichier(socketFichier, pNomUser);

				} catch (final Exception e) {
					try {
						/**
						 * Lorsque la connexion en mode client echoue, on se connecte en mode serveur.
						 */

						serveurSocket = new ServerSocket(portDistant);
						socket = serveurSocket.accept();
						socketFichier = serveurSocket.accept();
						mode.set(2);

						serveurMessagerieCourant = new ServeurMessagerie(socket, pNomUser);
						serveurFichierCourant = new ServeurFichier(socketFichier, pNomUser);

					} catch (final IOException e1) {
						Platform.runLater(new Runnable() {

							public void run() {
								new ExceptionPersonnalisee("Erreur connexion", e.getMessage());
							}

						});
					}
				}
			}
		});

		threadBoutonConnexion.start();
	}

	public ClientMessagerie getClientMessagerieCourant() {
		return clientMessagerieCourant;
	}

	public IntegerProperty getMode() {
		return mode;
	}

	public void setMode(IntegerProperty mode) {
		this.mode = mode;
	}

	public void setClientMessagerieCourant(ClientMessagerie clientMessagerieCourant) {
		this.clientMessagerieCourant = clientMessagerieCourant;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public ServeurMessagerie getServeurMessagerieCourant() {
		return serveurMessagerieCourant;
	}

	public ServeurFichier getServeurFichierCourant() {
		return serveurFichierCourant;
	}

	public ClientFichier getClientFichierCourant() {
		return clientFichierCourant;
	}

	public void setClientFichierCourant(ClientFichier clientFichierCourant) {
		this.clientFichierCourant = clientFichierCourant;
	}
	public ServerSocket getServeurSocket() {
		return serveurSocket;
	}

	public void setServeurSocket(ServerSocket serveurSocket) {
		this.serveurSocket = serveurSocket;
	}

	public Socket getSocketFichier() {
		return socketFichier;
	}

	public void setSocketFichier(Socket socketFichier) {
		this.socketFichier = socketFichier;
	}

}
