package Modele;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import Controleur.ControleurVue;
import javafx.application.Platform;

/**
 * Classe contenant toute les methodes utiles pour la communication entre deux
 * machines connectees.
 *
 * @author akimsoule
 *
 */
public class UtilitaireCommunication {

	/**
	 * Une liste qui contriendra le soket ainsi que tous les objet IO ayant permis
	 * la communication. Ils seront close lors de la fermetture de l'application.
	 */
	public static ArrayList<Object> elementsDeConnexion = new ArrayList<>();

	/**
	 * Methode permettant d'envoyer un fichier.
	 *
	 * @param cheminAbsoluSource Le chemin absolu du fichier.
	 * @param clientSocket       Le socket de communication.
	 */
	public static void envoyerFichier(String cheminAbsoluSource, Socket clientSocket) {
		elementsDeConnexion.add(clientSocket);
		FileObject fileObject = new FileObject(cheminAbsoluSource);

		File file = new File(cheminAbsoluSource);
		if (file.isFile()) {
			DataInputStream diStream = null;
			try {
				diStream = new DataInputStream(new FileInputStream(file));
				long len = (int) file.length();
				byte[] fileBytes = new byte[(int) len];
				int read = 0;
				int numRead = 0;
				while (read < fileBytes.length
						&& (numRead = diStream.read(fileBytes, read, fileBytes.length - read)) >= 0) {
					read = read + numRead;
				}
				fileObject.setTaille(len);
				fileObject.setDonnees(fileBytes);
				fileObject.setReussiteTransformationObjet(true);
			} catch (Exception e) {
				e.printStackTrace();
				fileObject.setReussiteTransformationObjet(false);
			}
		} else {
			Platform.runLater(() -> {
				new ExceptionPersonnalisee("Erreur fichier", "Chemin introuvale");
			});

			fileObject.setReussiteTransformationObjet(false);
		}

		if (fileObject.isReussiteTransformationObjet()) {
			try {
				final FileObject finalFileObject = fileObject;

				ObjectOutputStream outputStream = new ObjectOutputStream(clientSocket.getOutputStream());
				outputStream.writeObject(fileObject);
				Platform.runLater(() -> {
					ControleurVue.observableListInformations
							.add("Fichier " + finalFileObject.getNomFichier() + " envoye");
				});

				Thread.sleep(3000);

			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * Methode permettant de recevoir un fichier.
	 *
	 * @param clientSocket Le socket de communication.
	 */
	public static void receptionFichier(Socket clientSocket) {
		elementsDeConnexion.add(clientSocket);
		FileObject fileObectRecu = null;
		ObjectInputStream inputStream = null;
		boolean connected = true;
		while (connected) {
			try {
				inputStream = new ObjectInputStream(clientSocket.getInputStream());
				fileObectRecu = (FileObject) inputStream.readObject();
				if (!fileObectRecu.isReussiteTransformationObjet()) {
					System.out.println("Error occurred ..So exiting");

				} else {
					if (!new File(fileObectRecu.getCheminAbsoluDestinationReceptacle()).exists()) {
						new File(fileObectRecu.getCheminAbsoluDestinationReceptacle()).mkdirs();
					}

					File fileDest = new File(fileObectRecu.getCheminAbsoluDestination());
					FileOutputStream fileOutputStream = new FileOutputStream(fileDest);
					fileOutputStream.write(fileObectRecu.getDonnees());
					fileOutputStream.flush();
					fileOutputStream.close();
					final FileObject finalFileObject = fileObectRecu;
					Platform.runLater(() -> {
						ControleurVue.observableListInformations
								.add("Fichier " + finalFileObject.getNomFichier() + " reçu !");
					});

					Thread.sleep(3000);
				}

			} catch (IOException e) {
				connected = false;
			} catch (ClassNotFoundException e) {
				connected = false;
			} catch (InterruptedException e) {
				connected = false;
			}
		}

	}

	/**
	 * Methode permettant d'envoyer un message.
	 *
	 * @param pOut     Le PrintWriter pour l'envoi de message.
	 * @param message  Le contenu du message.
	 * @param userName Le nom de l'utilisateur.
	 */
	public static void envoyerMessage(PrintWriter pOut, String message, String userName) {
		if (pOut != null && message != null) {
			pOut.println(userName + " : " + message);
			pOut.flush();
			final String msg = userName + " : " + message;
			Platform.runLater(new Runnable() {
				public void run() {
					ControleurVue.observableListMessage.add(msg);

				}

			});
		}
	}

	/**
	 * Methode permettant de recevoir un message.
	 *
	 * @param in  Le Buffered pour la lecture de message
	 * @param out Le PrintWriter pour la fermetteur de la reception.
	 */
	public static void receptionMessage(BufferedReader in, PrintWriter out) {

		try {
			String msg = "";
			msg = in.readLine();
			while (msg != null) {
				final String message = msg;
				Platform.runLater(new Runnable() {
					public void run() {
						ControleurVue.observableListMessage.add(message);

					}

				});

				msg = in.readLine();
			}

			out.close();

		} catch (final IOException e) {
			Platform.runLater(new Runnable() {
				public void run() {
					new ExceptionPersonnalisee("Erreur reception message", e.getMessage());

				}

			});

		}
	}

	/**
	 * Methode permettant de closer la connection et de fermer le IO ayant servis a
	 * la connection.
	 *
	 * @param elementsDeConnexion L'arrayList d'objet contenant les elements de
	 *                            communication.
	 */
	public static void closeConnection(final ArrayList<Object> elementsDeConnexion) {

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {

					for (int i = 0; i < elementsDeConnexion.size(); i++) {
						if (elementsDeConnexion.get(i) != null) {
							if (elementsDeConnexion.get(i).getClass() == Socket.class) {
								((Socket) elementsDeConnexion.get(i)).close();
							}
						}
					}

				} catch (Exception e) {
				}

			}
		}).start();

	}

	/**
	 * Methode permettant d'envoyer une notification
	 *
	 * @param printOut Le PrintWriter pour l'envoi de message.
	 * @param nomUser  Le nom de l'utilisateur.
	 */
	public static void notification(PrintWriter printOut, String nomUser) {
		if (printOut != null) {
			printOut.println(nomUser + " : connecte");
			elementsDeConnexion.add(printOut);
			printOut.flush();

		}
	}

	/**
	 * Methode permettant de recevoir une notification.
	 *
	 * @param in  Le BufferedReader pour la lecture.
	 * @param out Le PrintWriter pour l'ecriture.
	 */
	public static void recevoirNotification(BufferedReader in, PrintWriter out) {
		try {
			String msg = "";
			msg = in.readLine();

			elementsDeConnexion.add(in);
			elementsDeConnexion.add(out);
			final String message = msg;
			Platform.runLater(new Runnable() {
				public void run() {
					ControleurVue.observableListInformations.add(message);
				}
			});
		} catch (final IOException e) {
			Platform.runLater(new Runnable() {
				public void run() {
					new ExceptionPersonnalisee("Erreur reception message", e.getMessage());

				}

			});

		}
	}

}
