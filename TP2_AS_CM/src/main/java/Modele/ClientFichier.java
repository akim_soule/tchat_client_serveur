package Modele;

import java.net.Socket;

/**
 * Classe permettant de definir un client fichier servant a la transmission et a la transmission
 * de fichier uniquement. Elle a pour principal attribut un socket et le nom de l'utilisation.
 * @author akimsoule
 *
 */
public class ClientFichier {

	/**
	 * Le nom de l'utilisateur
	 */
	private String nomUser;
	/**
	 * Le socket permettant a la transmission de l'information
	 */
	private Socket socket;

	/**
	 * Le constructeur de la classe permettant de definir le socket.
	 * Il permet egalement d'etre en mode reception de fichier.
	 * @param pClientSocket
	 * Le socket de transimission
	 * @param pNomUser
	 * Le nom du user.
	 */
	public ClientFichier(Socket pClientSocket, String pNomUser) {
		socket = pClientSocket;
		setNomUser(pNomUser);
		receptionFichier();
	}

	/**
	 * Methode permettant d'envoyer un fichier.
	 * @param chemin
	 * Le chemin du fichier
	 */
	public void envoiFichier(String chemin) {
		final String cheminFinal = chemin;
		new Thread(new Runnable(){
			public void run() {
				UtilitaireCommunication.envoyerFichier(cheminFinal, socket);
			}

		}).start();;
	}

	/**
	 * Methode permettant de recevoir un fichier.
	 */
	public void receptionFichier() {

		new Thread(new Runnable(){
			public void run() {
				UtilitaireCommunication.receptionFichier(socket);
			}

		}).start();
	}


	public String getNomUser() {
		return nomUser;
	}

	public void setNomUser(String nomUser) {
		this.nomUser = nomUser;
	}



}
